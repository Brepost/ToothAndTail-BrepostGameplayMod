Dim filesys
Set filesys = CreateObject("Scripting.FileSystemObject")
varPathCurrent = filesys.GetParentFolderName(WScript.ScriptFullName)
varPathParent = filesys.GetParentFolderName( filesys.GetParentFolderName(varPathCurrent) ) 'grandParent of current path

Dim ExtractTo
ExtractTo = varPathParent & "\content"
Zipfile=varPathCurrent & "\BrepostBalanceMod.zip"
WScript.echo(ExtractTo)
WScript.echo(Zipfile)
Call UnzipFolder(ExtractTo, Zipfile)

Function UnzipFolder(FoldPath, ZipFileName)
    Set objShell = CreateObject( "Shell.Application" )
    Set objSource = objShell.NameSpace(ZipFileName).Items()
    Set objTarget = objShell.NameSpace(FoldPath)
    intOptions = 256
	objTarget.CopyHere objSource, intOptions
End Function