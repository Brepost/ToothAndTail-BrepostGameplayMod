@echo off
For /f %%i in ('date /t') do (set mydate=%%i)
For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set mytime=%%a-%%b)

set backupDir=..\backups\%mydate%_%mytime%
set dataDir=..\..\content\data
set texDir=..\..\content\textures\actors

mkdir %backupDir%
echo Moving %dataDir% directory to %backupDir% ...
move %dataDir% %backupDir%

echo Copying %texDir% directory to %backupDir% ...
mkdir %backupDir%\textures\actors
xcopy /s %texDir% %backupDir%\textures\actors
mkdir %backupDir%\textures\portraits
xcopy /s %texDir% %backupDir%\textures\portraits

mkdir %dataDir%
echo Un-zipping mod files ...
@REM unzip -o BrepostGameplayMod.zip -d %dataDir%
@REM Call :UnZipFile %dataDir% BrepostGameplayMod.zip
cscript /B unzip.vbs
