@echo off
For /f %%i in ('date /t') do (set mydate=%%i)
For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set mytime=%%a-%%b)

set backupDir=..\backups\%mydate%_%mytime%
set dataDir=..\..\content\data

mkdir %backupDir%
echo Moving %dataDir% directory to %backupDir% ...
move %dataDir% %backupDir%

mkdir %dataDir%
echo Un-zipping mod files ...
cscript /B unzip.vbs
