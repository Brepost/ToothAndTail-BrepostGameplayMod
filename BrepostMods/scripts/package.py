import os
import shutil
import zipfile
import sys

os.chdir("..")

# PACKAGE = "BrepostGM_install.zip"
MODSDIR = "mods"
# README = "README"

# MODFILES = "data.zip"

def package(modData, modName):
    modDir = os.path.join(MODSDIR, modName)
    if not modData.endswith("zip"):
        print("Zipping mod files ...")
        try:
            os.delete(zipFilePath)
        except Exception:
            pass  # Zip file did not exist
        shutil.make_archive(os.path.join(modDir, modName), 'zip', modData)
        modData = os.path.join(modDir, modName + ".zip")
    # shutil.copyfile(modData, os.path.join(modDir, modName + ".zip"))
    
    with zipfile.ZipFile(modName + "_install.zip", 'w', zipfile.ZIP_DEFLATED) as installZip:
        for root, dirs, files in os.walk(modDir):
            for file in files:
                if file == "README":
                    installZip.write(os.path.join(root, file), file)
                else:
                    installZip.write(os.path.join(root, file))

if __name__ == "__main__":
    # Compute arg
    try:
        modFiles = sys.argv[1]
        modName = sys.argv[2]
        package(modFiles, modName)
        print("Done.")
    except IndexError:
        print "Error. You must pass the path to the mod's zip file"
    
