import os
import sys
import datetime
import shutil
import zipfile
import urllib

# Run this file using command "python patch_tnt.py #MODNAME#.zip" with #MODNAME# being the path to the mod you want to install
# To update the mod, run "python patch_tnt.py UPDATE"

# You can also run "python patch_tnt.py RESTORE" which will restore the game using the last backup
# You can also run "python patch_tnt.py UPDATE #MODDIR#" which will update the game using #MODDIR#'s content

# /!\ This file must be inside a folder named "mod" in the ToothAndTail installation folder, and run from that directory
# This program will create a "backups" folder in the working directory, and backup the game data in a specific directory there

BACKUPS = os.path.join("..", "..", "backups")
GAMECONTENT = os.path.join("..", "..", "..", "content")
GAMEDATA = os.path.join(GAMECONTENT, "data")
GAMETEXTURES = os.path.join(GAMECONTENT, "textures")
MODFILE = os.path.join("..", "data.zip")
MODOUTPUT = os.path.join("..", "mods")

def apply(modFile, backup=True):
    currentTime = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")

    if(backup):
        if not os.path.exists(BACKUPS):
            os.mkdir(BACKUPS)
        backupFolder = os.path.join(BACKUPS, currentTime)
        try:
            os.mkdir(backupFolder)
        except Exception:
            print "Couldn't create folder %s. Already exists ?" % backupFolder
            try:
                shutil.rmtree(backupFolder)
                os.mkdir(backupFolder)
            except Exception:
                print "Couldn't create backup folder. Aborting"
                return

        # Copy the contents of the TnT data folder into the backups dir
        print("Making a backup ...")
        shutil.copytree(GAMEDATA, os.path.join(backupFolder, "data"))
        
    # Delete TnT data folder
    print("Deleting TnT data folder ...")
    try:
        shutil.rmtree(GAMEDATA)
    except Exception:
        pass
        
    # If a zip, extract into TnT data folder
    if modFile.endswith("zip"):
        print("Extracting mod files ...")
        with zipfile.ZipFile(modFile, 'r') as mod:
            mod.extractall(GAMECONTENT)
    
    # If a folder, copy in TnT data folder
    else:
        print("Copying mod files ...")
        shutil.copytree(os.path.join(modFile, "data"), GAMEDATA)
        try:
            for root, _, files in os.walk(os.path.join(modFile, "textures")):
                for file in files:
                    src = os.path.join(root, file)
                    ind = root.find("textures")
                    try:
                        location = root[ind+9:]
                        dst = os.path.join(GAMETEXTURES, location, file)
                    except Exception:
                        # dst = os.path.join(GAMETEXTURES, root.rsplit(os.sep, 1)[1], file)
                        dst = os.path.join(GAMETEXTURES, file)
                    print src, dst
                    try:
                        os.remove(dst)
                    except Exception:
                        pass
                    shutil.copyfile(src, dst)
        except Exception:
            print "Error"
            pass  # No textures folder
    
def restore():
    # Get the last backup
    backups = sorted(os.listdir(BACKUPS), reverse=True)
    backup = backups[0]
    
    # Copy the contents of the backup folder into the TnT data folder
    print("Copying backup files ...")
    copytree(os.path.join(BACKUPS, backup), GAMECONTENT)
    
def copytree(src, dst):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d)
        else:
            shutil.copy(s, d)
    
def update():
    # Retrieve the content of https://gitlab.com/Brepost/ToothAndTail-BrepostGameplayMod/raw/master/BrepostMods/mods/BrepostGameplayMod/BrepostGameplayMod.zip
    urllib.urlretrieve("https://gitlab.com/Brepost/ToothAndTail-BrepostGameplayMod/raw/master/BrepostMods/mods/BrepostGameplayMod/BrepostGameplayMod.zip", "BrepostGameplayMod.zip")
    # Then update the files with its content
    apply("BrepostGameplayMod.zip", False)
       
if __name__ == "__main__":
    # Compute arg
    try:
        modFilePath = sys.argv[1]
    except IndexError:
        print "Error. You must pass the path to the mod's zip file"
    if modFilePath == "RESTORE":
        restore()
    elif modFilePath == "UPDATE":
        try:
            modFilePath = sys.argv[2]
            apply(modFilePath, False)
        except IndexError:
            update()
    else:
        apply(modFilePath)
    print("Done.")
